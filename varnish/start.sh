#!/bin/bash

sed -i "s/TARGET_HOST/${VARNISH_TARGET_HOST}/g" /etc/varnish/default.vcl
sed -i "s/TARGET_PORT/${VARNISH_TARGET_PORT}/g" /etc/varnish/default.vcl

# Start varnish and log
varnishd -j unix,user=varnish -f /etc/varnish/default.vcl -s malloc,64m -a :${VARNISH_HOST_PORT}

#varnishncsa
varnishlog