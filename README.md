## Building containers
* Register at magento.com and get download credentials into `auth.json`    
* Create `.env` file
* Get the sources into /app  

```
        docker-compose run -u $UID composer create-project \  
            --ignore-platform-reqs \  
            --no-dev \  
            --repository=https://repo.magento.com/ \  
            magento/project-community-edition ./app 2.3.*  
```  

* Build the image with all required dependencies  
```
        docker-compose build app
        docker-compose build cache
```


## Running Magento locally with Docker Compose
* Create volume folder in Docker host and up application  
```
        mkdir -p ./data/app ./data/db 
        cp -r ./app/app/app/etc ./data/app
        cp -r  ./app/app/pub/media ./data/app
        docker-compose up -d db
        sleep 30 # We need to let DB container initialize properly before magento container tries to interact with it
        docker-compose up -d app
        docker-compose up -d cache
```  
* And go to  [http://localhost](http://localhost)


## Setting up the container cluster in GCP
* Get an account in GCP
* Apply for a trial version and get the 300 USD credit
* Create a project - name it e.g. sample-project     
* In your local env setup setup GCloud SDK
* Set default Region and default Zone (in Finland it is europe-north1/europe-north1-a)
* Run `gcloud config list` to confirm, output should be like 
```
        [compute]
        region = europe-north1
        zone = europe-north1-a
        [core]
        account = artur.ismailov@gmail.com
        disable_usage_reporting = True
        project = artur-terraform-admin
```
* Build container images and push to the remote registry:
```
        docker build -t gcr.io/artur-terraform-admin/magento-nginx-fpm:0.4 ./app/
        docker push gcr.io/artur-terraform-admin/magento-nginx-fpm:0.4 
        docker build -t gcr.io/artur-terraform-admin/varnish:0.4 ./varnish/
        docker push gcr.io/artur-terraform-admin/varnish:0.4
        docker build -t gcr.io/artur-terraform-admin/cache-invalidator:0.1 ./cache-invalidator/
        docker push gcr.io/artur-terraform-admin/cache-invalidator:0.1
```
* Create a CloudSQL DB with replica (or without for simplicity)  

>   CloudSQL instance is accessible from the public IP unless a private VPC peering is established. For testing purposes public IP is being used. Configure the instance to accept connections from selected IPs, I have added an approx CIDR range for IPs of my cluster nodes. Might be a dirty unsecure workaround.

```
        gcloud sql instances create magento-db-instance --tier=db-f1-micro --region=europe-north1
        gcloud sql instances patch magento-db-instance --authorized-networks=35.228.0.0/16
        gcloud sql instances describe magento-db-instance | grep ipAddress:
        gcloud sql databases create arturmagento --instance=magento-db-instance
        gcloud sql users set-password root --host=% --instance=magento-db-instance --password=your-password
        gcloud sql users create magento-user --instance=magento-db-instance --password=your-password
```
* Create a database, e.g. `magento-database`, fill in the values for DB in `kubernetes/configmaps/magento-configmap.yaml` and in `kubernetes/configmaps/magento-configmap-secrets.yaml`
* Create a Kubernetes cluster
```
        gcloud container clusters create magento-cluster
```
* In case you're switching between local cluster and remote in GCP you need to:
```
        kubectl config get-contexts
        kubectl config use-context gke_artur-terraform-admin_europe-north1-a_magento-cluster
        gcloud container clusters get-credentials magento-cluster
```
* Deploy Varnish to the cluster
```
        kubectl apply -f kubernetes/configmaps/varnish-configmap.yaml
        kubectl apply -f kubernetes/configmaps/varnish-configmap-secrets.yaml
        kubectl apply -f kubernetes/deployments/varnish-deployment.yaml
        kubectl apply -f kubernetes/services/varnish-service.yaml
        sleep 60 # It takes time to create a service with Loadbalancing and to generate an LB IP
```

>    Note that you have to supply secret values in base64, also don't forget to replace values in git with your actual values

* Get the external IP of the Varnish service after its created with:  
```
        kubectl get services --field-selector=metadata.name=varnish-service -o custom-columns=EXTERNAL-IP:.status.loadBalancer.ingress[].ip
```

>    Magento needs a hardcoded base_url, when Magento is initialized on first launch it installs the DB with base_url provided. Otherwise it won't work.

* Deploy Magento to the cluster with values from the prev steps
```
        cat ./kubernetes/configmaps/magento-configmap.yaml \
        | sed 's/DB_HOST : .*/DB_HOST : "mysql"/g' \
        | sed 's/DB_DATABASE : .*/DB_DATABASE : "arturmagento"/g' \
        | sed 's/M2SETUP_BASE_URL : .*/M2SETUP_BASE_URL : "http:\/\/35.228.49.81\/"/g' \
        | kubectl apply -f -
        kubectl apply -f kubernetes/configmaps/magento-configmap-secrets.yaml
        kubectl apply -f kubernetes/deployments/magento-deployment.yaml
        kubectl apply -f kubernetes/services/magento-service.yaml
```
* Deploy Magento to the cluster
```
        kubectl apply -f kubernetes/deployments/magento-admin-deployment.yaml
        sleep 60
        kubectl apply -f kubernetes/deployments/magento-deployment.yaml
        kubectl apply -f kubernetes/services/magento-service.yaml
        kubectl delete -f kubernetes/deployments/varnish-deployment.yaml
        kubectl apply -f kubernetes/deployments/varnish-deployment.yaml
```
* Deploy Varnish Cache invalidator to the cluster
```
        kubectl apply -f kubernetes/configmaps/cache-invalidator-configmap.yaml
        kubectl apply -f kubernetes/deployments/cache-invalidator-deployment.yaml
        kubectl apply -f kubernetes/services/cache-invalidator-service.yaml
```

## Setting up the container cluster with Minikube
* Install and initialize Minikube (I've used Virtualbox) 

>    Magento running on mininkube installed with default settings was very slow for me. Minikube was slow, crashing from time to time and inconvenient. On a Mac I have changed the system configuration of the VirtualBox for minkube machine - CPU to 4, RAM to 4G. That helped to get system to the reasonable performance.

* Magento and Varnish images in gcr.io must be public for minikube to access them. We can build them locally in Minikube context with the following commands. Then Minikube will pull them from the local machine. 
```
        eval $(minikube docker-env)
        docker build -t gcr.io/artur-terraform-admin/magento-nginx-fpm:0.4 ./app/
        docker build -t gcr.io/artur-terraform-admin/varnish:0.4 ./varnish/
        docker build -t gcr.io/artur-terraform-admin/cache-invalidator:0.1 ./cache-invalidator/
```
* Set context to the proper cluster if you have many
```
        kubectl config get-contexts
        kubectl config use-context minikube
```
* Deploy configMaps, secrets, deployment and service for Varnish
```
        kubectl apply -f ./kubernetes/configmaps/varnish-configmap.yaml
        kubectl apply -f ./kubernetes/configmaps/varnish-configmap-secrets.yaml
        kubectl apply -f kubernetes/deployments/varnish-deployment.yaml
        kubectl apply -f kubernetes/services/varnish-service.yaml
```  

>    Note that you have to supply secret values in base64, also don't forget to replace values in git with your actual values

* Get the external IP of the Varnish service after its created with:
```
        minikube service varnish-service
```

>    Magento needs a hardcoded base_url, when Magento is initialized on first launch it installs the DB with base_url provided. Otherwise it won't work.

* Open another shell session and deploy configmaps for Magento and local MySQL with Varnish Service Loadbalancing http address from previous step
```
        cat ./kubernetes/configmaps/magento-configmap.yaml \
        | sed 's/DB_HOST : .*/DB_HOST : "mysql"/g' \
        | sed 's/DB_DATABASE : .*/DB_DATABASE : "arturmagento"/g' \
        | sed 's/M2SETUP_BASE_URL : .*/M2SETUP_BASE_URL : "http:\/\/192.168.99.101:30576\/"/g' \
        | kubectl apply -f -
        kubectl apply -f ./kubernetes/configmaps/magento-configmap-secrets.yaml
```
* Create a MySQL DB locally
```
        kubectl apply -f ./kubernetes/local.yaml  
```
* Deploy Magento to the cluster
```
        kubectl apply -f kubernetes/deployments/magento-admin-deployment.yaml
        sleep 60
        kubectl apply -f kubernetes/deployments/magento-deployment.yaml
        kubectl apply -f kubernetes/services/magento-service.yaml
        kubectl delete -f kubernetes/deployments/varnish-deployment.yaml
        kubectl apply -f kubernetes/deployments/varnish-deployment.yaml
```
* Deploy Varnish Cache invalidator to the cluster
```
        kubectl apply -f kubernetes/configmaps/cache-invalidator-configmap.yaml
        kubectl apply -f kubernetes/deployments/cache-invalidator-deployment.yaml
        kubectl apply -f kubernetes/services/cache-invalidator-service.yaml
```

## Cleanup (for each kubectl context)
```
        kubectl delete -f ./kubernetes/services/magento-service.yaml
        kubectl delete -f ./kubernetes/services/varnish-service.yaml
        kubectl delete -f ./kubernetes/deployments/magento-deployment.yaml
        kubectl delete -f ./kubernetes/deployments/varnish-deployment.yaml
        kubectl delete -f ./kubernetes/configmaps/magento-configmap-secrets.yaml
        kubectl delete -f ./kubernetes/configmaps/magento-configmap.yaml
        kubectl delete -f ./kubernetes/configmaps/varnish-configmap.yaml
        kubectl delete -f ./kubernetes/configmaps/varnish-configmap-secrets.yaml
```
* Minikube only
```
        kubectl delete -f ./kubernetes/local.yaml
        minikube stop
        minikube delete
```
* GCP only
```
        gcloud container clusters list
        gcloud container clusters delete magento-cluster

        gcloud sql databases delete arturmagento --instance magento-db-instance
        gcloud sql instances delete magento-db-instance
```