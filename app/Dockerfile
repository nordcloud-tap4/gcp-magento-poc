FROM ubuntu:16.04

ENV APP_ROOT /var/www

### Install
RUN apt-get update \
  && apt-get -y install --no-install-recommends software-properties-common zip locales curl cron\
  && locale-gen en_US.UTF-8 && export LANG=en_US.UTF-8 \
  && add-apt-repository -y ppa:ondrej/php


# PHP install
# dependecy like https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements-tech.html
RUN apt-get update && apt-get -y install \
	php7.2 \
	php7.2-fpm \
	php7.2-curl \
	php7.2-dom \
	php7.2-gd \
	php7.2-intl \
	php7.2-mbstring \
	php7.2-soap \
	php7.2-zip \
	php7.2-mysql \
	php7.2-bcmath \
  && sed -i -e "s/user\s*=\s*www-data/user = symfony;/g" /etc/php/7.2/fpm/pool.d/www.conf \
  && sed -i -e "s/listen.owner\s*=\s*www-data/listen.owner = symfony;/g" /etc/php/7.2/fpm/pool.d/www.conf \
  && sed -i -e "s/listen.owner\s*=\s*www-data/listen.owner:wq = symfony;/g" /etc/php/7.2/fpm/pool.d/www.conf \
  && sed -i -e "s/;\?daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.2/fpm/php-fpm.conf

# Nginx install
RUN apt-get update && apt-get -y install nginx \
  && echo "\ndaemon off;" >> /etc/nginx/nginx.conf \
  && sed -i -e "s/user\s*www-data;/user symfony;/g" /etc/nginx/nginx.conf


# Clearing
RUN apt-get clean \
  && rm -rf /var/www \
  && adduser --disabled-password --gecos "" symfony

ENV M2SETUP_CACHE_HOST false
# admin type will be used for installation and configuration, all other types will skip installation and act in serving node (worker nodes)
ENV M2SETUP_POD_TYPE admin

# Configure nginx
COPY config/nginx/default /etc/nginx/sites-available/default
COPY ./app ${APP_ROOT}
COPY ./docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR ${APP_ROOT}

# Expose ports
EXPOSE 80

ENTRYPOINT ["/docker-entrypoint.sh"]
