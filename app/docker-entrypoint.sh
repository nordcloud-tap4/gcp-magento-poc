#!/bin/bash

# Check if this pod is supposed to be an admin pod, if so - create crons within it per project requirements
if [ ${M2SETUP_POD_TYPE} == "admin" ]; then

	php $APP_ROOT/bin/magento setup:config:set \
			--db-host $DB_HOST \
			--db-name $DB_DATABASE \
			--db-user $DB_USER \
			--db-password $DB_PASSWORD

	echo -e "Checking status of Magento DB"

	php $APP_ROOT/bin/magento setup:db:status

	case $? in
		"0")
			echo "Magento DB is 0 (Normal state), no action needed"
			;;
		1|42)
			echo "Magento DB status is 1 needs install or upgrade or table not exist 42 need install , we'll run install"
			php $APP_ROOT/bin/magento setup:install \
					--db-host $DB_HOST \
					--db-name $DB_DATABASE \
					--db-user $DB_USER \
					--db-password $DB_PASSWORD \
					--base-url=$M2SETUP_BASE_URL \
					--backend-frontname=$M2SETUP_BACKEND_FRONTNAME \
					--admin-firstname=$M2SETUP_ADMIN_FIRSTNAME \
					--admin-lastname=$M2SETUP_ADMIN_LASTNAME \
					--admin-email=$M2SETUP_ADMIN_EMAIL \
					--admin-user=$M2SETUP_ADMIN_USER \
					--admin-password=$M2SETUP_ADMIN_PASSWORD
			;;
		"2")
			echo "Magento DB status is 2, upgrade is needed, we'll run install"
			php bin/magento setup:upgrade
			;;
	esac

	if [ ${M2SETUP_CACHE_HOST} != false ]; then \
		echo "Setting up external cache host at default port"
		php $APP_ROOT/bin/magento config:set --scope=default --scope-code=0 system/full_page_cache/caching_application 2
		php $APP_ROOT/bin/magento setup:config:set --http-cache-hosts=${M2SETUP_CACHE_HOST}
	fi

	chown -R symfony:symfony \
		$APP_ROOT/var \
		$APP_ROOT/generated \
		$APP_ROOT/pub/static \
		$APP_ROOT/pub/media \
		$APP_ROOT/app/etc
	chmod -R u+w \
		$APP_ROOT/var \
		$APP_ROOT/generated \
		$APP_ROOT/pub/static \
		$APP_ROOT/pub/media \
		$APP_ROOT/app/etc
	chmod u+x bin/magento

	#TODO add crons
	runuser -l symfony -c "php $APP_ROOT/bin/magento cron:install"
	# debug output
	crontab -u symfony -l
fi

service php7.2-fpm start
service cron start
nginx -t
nginx

exec "$@"
