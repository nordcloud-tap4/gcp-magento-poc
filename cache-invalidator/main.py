#!/usr/bin/env python
import os, logging, sys
from http.server import HTTPServer
from server import Server


PORT_NUMBER = os.environ.get('CI_LISTEN_PORT')

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='[%(levelname)s]\t %(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logger = logging.getLogger()

if __name__ == '__main__':
    httpd = HTTPServer(('', int(PORT_NUMBER)), Server)
    logger.info('Server up on port - %s', PORT_NUMBER)
    #print(time.asctime(), 'Server up on port - %s' % PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logger.info('Server down - %s', PORT_NUMBER)