import os, requests, logging, sys
from http.server import BaseHTTPRequestHandler
from kubernetes import client, config

LABEL_SELECTOR = os.environ.get('CI_TARGET_PODS_LABEL_SELECTOR')
TARGET_PORT = os.environ.get('CI_TARGET_PODS_PORT')

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='[%(levelname)s]\t %(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
logger = logging.getLogger()

class Server(BaseHTTPRequestHandler):
    def do_GET(self):
        logger.info("do_GET is called")
        self.multicast_to_pods()
        self.respond()
    def do_PURGE(self):
        logger.info("do_PURGE is called")
        self.multicast_to_pods()
        self.respond()
    def handle_http(self, status, content_type):
        self.send_response(status)
        self.send_header('Content-type', content_type)
        self.end_headers()
        return bytes("OK", "UTF-8")
    def respond(self):
        content = self.handle_http(200, 'text/html')
        self.wfile.write(content)
        return
    def multicast_to_pods(self):
        logger.info("multicasting request to the relevant pods")

        try:
            logger.debug("trying to load_incluster_config()")
            config.load_incluster_config()
        except:
            logger.debug("trying to load_cube_config()")
            config.load_kube_config()

        v1 = client.CoreV1Api()
        ret = v1.list_namespaced_pod(namespace='default', label_selector='%s' % LABEL_SELECTOR)

        for i in ret.items:
            pod_varnish_http_address = "http://" + i.status.pod_ip + ":" + TARGET_PORT + self.path
            logger.info("Sending PURGE request to %s (%s)", i.metadata.name, pod_varnish_http_address)
            response = requests.request(method="PURGE", url=pod_varnish_http_address, headers=self.headers,timeout=10)
            logger.debug("PURGE request returned %s", response.status_code)
        return
